;(function($, window, undefined) {

  if (!Date.now) {
      Date.now = function() { return new Date().getTime(); }
  }


	function format(format, token, id) {
		format = format.replace(/{token}/g, token);
		format = format.replace(/{campaignId}/g, id);
		return format;
	}


	
  $.fn.enparticipation = function(options) {

    var defaults = {
      id: [],
      format: false,
      live: false,
      update: 5000,
			token: ""
    },
				en_url = "https://act.greenpeace.org/ea-dataservice/data.service?service=EaDataCapture&token={token}&campaignId={campaignId}&contentType=json&resultType=summary",
				total = 0,
				loaded = 0,
				received = 0,
				settings,
				selector = this
		

    function updateSelector(num) {
			loaded = loaded + 1;
			received = received + parseInt(num);

			if (loaded >= total) {
				(function(sum) {
					selector.each(function() {
						
						var $this = $(this);
						
						if (true === $this.data('superior_only')
							&& sum < $this.data('counter')) {
								return;
						}
						
						
						var res = sum;
						if (settings.format && typeof settings.format === "function") {
							res = settings.format(res);
						}
						$this
							.html(res)
							.data('counter', sum);
					});
					$(window).trigger('en_participation', {count: sum});
				})(received);

				received = 0;
				loaded = 0;
			}
    }


		function extractData(data) {
      var c = data.rows[0].columns;
      for (var i = 0, l = c.length; i < l; i++) {
        if (c[i].name === "participatingSupporters") {
          return parseInt(c[i].value);
        }
      }
      return 0;
    }


    function getData(ids) {

			for (var i = 0, l = ids.length; i < l; i++) {			
				$.get(
					format(en_url, settings.token, ids[i]),
					{v: (Date.now() / 1000 | 0)},
					function(data) {
						updateSelector( extractData( data ) );
					},
					"jsonp"
				);
			}
    }

    if (options === null)
    return;

    if (typeof options === "number" || typeof options === "string") {
      options = {id: options};
    }


    if (options.id) {
			switch (options.id) {
				case 'object':
					if ('undefined' == typeof options.id.length) {
						var ids = [];
						for (var i in options.id) {
							ids.push(i);
						}
						options.id = ids;
					}
					break;

				default:
					options.id = [parseInt(options.id)];
					break;
			}

			total = options.id.length;
			
    }
    else {
      return;
    }


    settings = $.extend(
      defaults,
      options
    );

		
    getData(settings.id);


		
    if (settings.live && settings.update) {
      setInterval(function() {
        getData(settings.id);
      }, settings.update);
    }

};

})(jQuery, window);
