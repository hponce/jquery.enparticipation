'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// If you want to recursively match all subfolders, use:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

	// Time how long tasks take. Can help when optimizing build times
	require('time-grunt')(grunt);

	// Load grunt tasks automatically
	require('load-grunt-tasks')(grunt);

	// Define the configuration for all the tasks
	grunt.initConfig({
		uglify: {
			options: {
				mangle: true
			},
			my_target: {
				files: {
					'dist/jquery.enparticipation.min.js': ['src/jquery.enparticipation.js']
				}
			}
		},

		copy: {
			dist: {
				files: [
					{
						expand: true,
						dot: true,
						cwd: 'src/',
						dest: 'dist/',
						src: [
							'jquery.enparticipation.js',
							'test.html'
						]
					}
				]
			}
		},

		clean: {
			dist: {
				files: [{
					dot: true,
					src: [
						'dist/*'
					]
				}]
			}
		}


  });


	grunt.registerTask('build', 'build', function(target) {
		grunt.task.run([
			'clean',
			'uglify',
			'copy'
		]);
	});


	grunt.registerTask('default', [
		'build'
	]);
};
